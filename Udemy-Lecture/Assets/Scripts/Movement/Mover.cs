using Core;
using UnityEngine;
using UnityEngine.AI;

namespace Movement
{
    public class Mover : MonoBehaviour, IAction
    {
        //[SerializeField] private Transform tartget;
        private static readonly int ForwardSpeed = Animator.StringToHash("ForwardSpeed");
        private NavMeshAgent _navMeshAgent;
        private Animator _animator;

        private void Start()
        {
            _animator = GetComponent<Animator>();
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        void Update()
        {
            UpdateAnimator();
        }
        
        // PlayerController 에서 사용되는 이동 함수(지면 좌클릭 때 실행됨)
        public void StartMoveAction(Vector3 destination)
        {
            GetComponent<ActionScheduler>().StartAction(this);
            //GetComponent<Fighter>().Cancel();   // 타겟 null => interface 사용으로 종속성 줄어들어 제거 가능(네임스페이스도 제거)
            MoveTo(destination);
        }
        
        // Fighter 에서 사용되는 이동 함수(적 좌클릭 때 실행됨)
        public void MoveTo(Vector3 destination)
        {
            _navMeshAgent.destination = destination;
            _navMeshAgent.isStopped = false;
        }

        public void Cancel()
        {
            _navMeshAgent.isStopped = true;
        }

        public void Ab(string a, string b)
        {
            
        }

        private void UpdateAnimator()
        {
            Vector3 velocity = _navMeshAgent.velocity;
            Vector3 localVelocity = transform.InverseTransformDirection(velocity);
            float speed = localVelocity.z;
            _animator.SetFloat(ForwardSpeed, speed);
        }
    }
}
