using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class ActionScheduler : MonoBehaviour
    {
        // 행동 사이에 종속성을 없애기 위한 인터페이스
        private IAction _currentAction;

        // MonoBehaviour 의 클래스를 계속 갈아끼움
        public void StartAction(IAction action)
        {
            // 같은 action의 연속이면 취소하고 실행하는 로직을 실행할 필요 없음
            if(_currentAction == action) return;
            // 현재의 액션이 존재할 때만 현재 action을 취소하고 다음 action을 실행함
            if (_currentAction != null)
            {
                _currentAction.Cancel();
            }
            _currentAction = action;
        }
    }
}

