using System.Collections;
using System.Collections.Generic;
using Combat;
using Core;
using Movement;
using UnityEngine;

namespace Control
{
    public class PlayerController : MonoBehaviour
    {
        private readonly RaycastHit[] _results = new RaycastHit[10]; // RaycastNonAlloc를 위한 변수

        private void Update()
        {
            // 전투와 이동을 완전히 구분하기 위해 bool값 처리. + 행동 순위 조정 (공격 판정이 먼저)
            if (InteractWithCombat2()) return;
            if (InteractWithMovement()) return;
        }

        private bool InteractWithCombat()
        {
            RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
            foreach (RaycastHit hit in hits)
            {
                CombatTarget target = hit.transform.GetComponent<CombatTarget>();
                if(target == null) continue;

                if (Input.GetMouseButtonDown(0))
                {
                    GetComponent<Fighter>().Attack(target);
                }
                // 리턴이 바로 위의 함수에 들어가면 마우스 클릭 했을 때만 값을 리턴
                // 지금 자리라면 마우스가 타겟 위에 위치하면 계속 값을 리턴
                // 나중에 전투 커서로 마우스 커서 전환할 듯? 이동 커서, 전투 커서 등등..
                return true;        
            }
            return false;
        }
        
        // RaycastNonAlloc 버전
        private bool InteractWithCombat2()
        {
            int hits = Physics.RaycastNonAlloc(GetMouseRay(), _results);

            for (int i = 0; i < hits; i++)
            {
                CombatTarget target = _results[i].transform.GetComponent<CombatTarget>();
                if(target == null) continue;

                if (Input.GetMouseButtonDown(0))
                {
                    GetComponent<Fighter>().Attack(target);
                }
                return true;
            }
            return false;
        }
        
        // 추후 등장할 마우스 커서(걷기 가능, 전투 가능 등)으로 유저에게 힌트를 주기 위한 전초 작업
        // = 인터페이스 디자인
        private bool InteractWithMovement()
        {
            
            
            RaycastHit hit;
            bool hasHit = Physics.Raycast(GetMouseRay(), out hit);
            if (hasHit)
            {
                if (Input.GetMouseButton(0))
                {
                    GetComponent<Mover>().StartMoveAction(hit.point);
                }
                return true;
            }
            return false;       // 마우스를 클릭하지 않으면 false
        }

        private static Ray GetMouseRay()
        {
            return Camera.main.ScreenPointToRay(Input.mousePosition);
        }
    }
}
