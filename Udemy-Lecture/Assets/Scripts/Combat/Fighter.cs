using System;
using System.Collections;
using System.Collections.Generic;
using Core;
using Movement;
using UnityEngine;

namespace Combat
{
    public class Fighter : MonoBehaviour, IAction
    {
        [SerializeField] private float weaponRange = 2f;
        
        private Transform _target;
        private Mover _mover;
        private Animator _animator;
        private static readonly int Attack1 = Animator.StringToHash("attack");

        private void Start()
        {
            _animator = GetComponent<Animator>();
            _mover = GetComponent<Mover>();
        }

        private void Update()
        {
            if(!_target) return;
            // Distance와 SqrMagnitude 사용
            //bool isInRange = (transform.position - _target.position).sqrMagnitude <= weaponRange * weaponRange;
            if (!GetIsInRange())
            {
                _mover.MoveTo(_target.position);
            }
            else
            {
                _mover.Cancel();
                _animator.SetTrigger(Attack1);
            }
        }

        private bool GetIsInRange()
        {
            return Vector3.Distance(transform.position, _target.position) < weaponRange;
        }

        // combatTarget은 PlayerController에서 적을 클릭 시 Attack()의 매개변수로 같이 호출됨
        public void Attack(CombatTarget combatTarget)
        {
            GetComponent<ActionScheduler>().StartAction(this);
            _target = combatTarget.transform;
            //StartCoroutine(UpdateAttack());
        }

        public void Cancel()
        {
            _target = null;

        }

        void Hit()
        {
            
        }
        
        
        /*IEnumerator UpdateAttack()
        {
            _mover.MoveTo(_target.position);
            yield return new WaitUntil(GetIsInRange);
            //yield return new WaitUntil(() => GetIsInRange());
            //yield return new WaitUntil(() => Vector3.Distance(transform.position, _target.position) < weaponRange);
            _mover.Cancel();
        }*/
    }
}