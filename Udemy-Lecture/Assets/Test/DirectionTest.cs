using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionTest : MonoBehaviour
{

    [SerializeField] private GameObject target = null;
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Test();
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            Test2();
        }
    }

    void Test()
    {
        Vector3 td = target.transform.TransformDirection(Vector3.forward);
        Debug.Log("TransformDirection : " + td);
    }

    void Test2()
    {
        Vector3 itd = target.transform.InverseTransformDirection(Vector3.forward);
        Debug.Log("InverseTransformDirection : " + itd);
    }

    void TdInputTest()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        Vector3 moveDir = new Vector3(h, 0f, v).normalized;
        Vector3 worldMoveDir = transform.TransformDirection(moveDir);
    }
}
